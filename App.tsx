import React, {useState} from 'react';
import {
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  useColorScheme,
  NativeModules,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {stringToNumber} from './helpers';

const operatorList = ['plus', 'minus'];
const {MathModule} = NativeModules;

const App = () => {
  const [result, setResult] = useState<null | number>(null);
  const [firstNumber, setFirstNumber] = useState<null | string>(null);
  const [secondNumber, setSecondNumber] = useState<null | string>(null);
  const [chosenOperator, setOperator] = useState('');
  const isDarkMode = useColorScheme() === 'dark';

  const buttonDisabled =
    !stringToNumber(firstNumber) || !stringToNumber(secondNumber);

  async function getData() {
    let operator =
      operatorList[Math.floor(Math.random() * operatorList.length)];
    setOperator(operator);
    const data = await MathModule.calculate(
      [Number(firstNumber), Number(secondNumber)],
      operator,
    );
    setResult(data);
  }

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <ScrollView contentContainerStyle={[backgroundStyle, styles.container]}>
        <TextInput
          keyboardType="numeric"
          style={styles.input}
          onChangeText={setFirstNumber}
        />
        <TextInput
          onChangeText={setSecondNumber}
          keyboardType="numeric"
          style={styles.input}
        />
        <Pressable
          disabled={buttonDisabled}
          style={styles.button}
          onPress={getData}>
          <Text style={styles.buttonText}>Submit</Text>
        </Pressable>
        <Text style={styles.title}>Result</Text>
        <Text>Your operation is {chosenOperator}</Text>
        <Text style={styles.resultText}>{result}</Text>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    width: '100%',
    height: 48,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 6,
    marginBottom: 10,
  },
  container: {
    padding: 16,
    height: '100%',
  },
  button: {
    width: '100%',
    height: 48,
    backgroundColor: '#00bfff',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#ffffff',
  },
  title: {
    textAlign: 'center',
    fontSize: 16,
    marginVertical: 20,
    fontWeight: 'bold',
  },
  resultText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 26,
  },
});

export default App;
