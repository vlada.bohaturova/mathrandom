package com.mathrandom;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import java.util.Map;
import java.lang.reflect.Array;
import java.util.HashMap;
import android.util.Log;





public class MathModule extends ReactContextBaseJavaModule {
   MathModule(ReactApplicationContext context) {
       super(context);
   }

    @Override
    public String getName() {
        return "MathModule";
    }


    @ReactMethod(isBlockingSynchronousMethod = true)
    public int calculate(ReadableArray arr, String operator) {
        int sum = 0;
        int i;
        if (operator.equals("plus")){
            for (i = 0; i < arr.size(); i++)
            sum +=  arr.getInt(i); 
            return sum;
        }
        if (operator.equals("minus")){
         return  arr.getInt(0) - arr.getInt(1);
        }

        return sum;
    }
}