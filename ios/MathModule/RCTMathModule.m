//
//  RCTMathModule.m
//  MathRandom
//
//  Created by vostapchuk on 17.03.2022.
//

#import "RCTMathModule.h"
#import "React/RCTBridgeModule.h"

@implementation RCTMathModule

RCT_EXPORT_MODULE(MathModule);

RCT_EXPORT_METHOD(calculate:(NSArray *)arrayOfNumbers operator:(NSString *)operator resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  NSInteger sum = 0;
  
  if([operator isEqualToString:@"plus"]) {
    for (NSNumber *num in arrayOfNumbers) {
      sum += [num intValue];
    }
//    resolve([NSNumber numberWithInt:sum]);
         }
  else if([operator isEqualToString:@"minus"]) {
//    sum = firstNumber-secondNumber;
    for (NSNumber *num in arrayOfNumbers) {
      sum =sum- [num intValue];
    }
   
         }
  
  

    resolve([NSNumber numberWithInt:sum]);
}


@end
