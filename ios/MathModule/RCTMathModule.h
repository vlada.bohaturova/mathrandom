//
//  RCTMathModule.h
//  MathRandom
//
//  Created by vostapchuk on 17.03.2022.
//

#ifndef RCTMathModule_h
#define RCTMathModule_h

#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>

@interface RCTMathModule : NSObject <RCTBridgeModule>
@end


#endif /* RCTMathModule_h */
