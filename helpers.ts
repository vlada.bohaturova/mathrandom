export const stringToNumber = (string: string | null) => {
  return string?.replace(/,/g, '.') || string;
};
